#include <GP2YDustSensor.h>
#include "pitches.h"

int ledVerde = 10;
int ledRojo = 6;
int ledAmarillo = 9;

const uint8_t SHARP_LED_PIN = 2;   // Sharp Dust/particle sensor Led Pin
const uint8_t SHARP_VO_PIN = A0;    // Sharp Dust/particle analog out pin used for reading 

GP2YDustSensor dustSensor(GP2YDustSensorType::GP2Y1010AU0F, SHARP_LED_PIN, SHARP_VO_PIN);
int melody[] = {
  NOTE_A3, NOTE_B3, NOTE_D4, NOTE_B4, NOTE_FS4, NOTE_FS4, NOTE_E4, NOTE_A3, NOTE_B3, NOTE_D4, NOTE_B4, NOTE_E4, NOTE_E4, NOTE_D4, NOTE_CS4, NOTE_B3
};

// note durations: 4 = quarter note, 8 = eighth note, etc.:
int noteDurations[] = {
  8, 8, 8, 8, 4, 4, 2, 8, 8, 8, 8, 4, 4, 8, 8, 2
};


void setup() {
  pinMode(ledVerde, OUTPUT);
  pinMode(ledRojo, OUTPUT);
  pinMode(ledAmarillo, OUTPUT);

  Serial.begin(9600);
  //dustSensor.setBaseline(0.4); // set no dust voltage according to your own experiments
  //dustSensor.setCalibrationFactor(1.1); // calibrate against precision instrument
  dustSensor.begin();
}

void loop() {

  Serial.print("Dust density: ");
  Serial.print(dustSensor.getDustDensity());
  Serial.print(" ug/m3; Running average: ");
  Serial.print(dustSensor.getRunningAverage());
  Serial.println(" ug/m3");
  delay(1000);

    int densidad = dustSensor.getDustDensity();
  if (densidad < 50)
  {
    digitalWrite(ledVerde, HIGH);
    digitalWrite(ledAmarillo, LOW);
    digitalWrite(ledRojo, LOW);

  }
    else if (50 <= densidad && densidad < 100)
  {
    digitalWrite(ledAmarillo, HIGH);
    digitalWrite(ledVerde, LOW);
    digitalWrite(ledRojo, LOW);
  }
    else if (100 <= densidad)
  {
    digitalWrite(ledRojo, HIGH);
    digitalWrite(ledAmarillo, LOW);
    digitalWrite(ledVerde, LOW);
    delay(50);

  for (int thisNote = 0; thisNote < 16; thisNote++) {
    // to calculate the note duration, take one second divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000 / noteDurations[thisNote];
    tone(8, melody[thisNote], noteDuration);

    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(8);
   }  }

}



//Esta la idea global:
//densidad = dustSensor.getDustDensity;
//if (x < densidad && densidad < y)
//{
  // encender led 1, buzzer hace sonido 1;
//}
//else if (x <= densidad && densidad < y)
//{
  // encender led 2, buzzer hace sonido 2;
//}
//else if (x <= densidad && densidad < y)
//{
  // encender led 3, buzzer hace sonido 3;
//}
//else if (x <= densidad && densidad < y)
//{
  // encender led 4, buzzer hace sonido 4;
//}
//else if (x <= densidad)
//{
  // encender led 5, buzzer hace sonido 5;
//}
//else
//{
  // apagar todo;
//}
